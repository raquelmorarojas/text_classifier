import nltk
from nltk.tokenize import word_tokenize
from nltk.probability import FreqDist
from nltk.corpus import stopwords
from nltk.stem.porter import *


class MyClassifier:
    def __init__(self, text):
        self.text = text

        self.stop_words = set(stopwords.words("spanish"))

        self.punctuation = [".", ",", "?", "!", "...", ";", ":"]

        self.topics = ["beca","créditos","práctica","carga", "trabajo", "laborales", "salir", "adelantarme",
			"académica", "atrasarme", "finalizar", "bloque", "cierre", "profesional", "avanzar", "requisito", "levantamiento",
            "inclusión", "cupo"]

        self.stemmer = PorterStemmer()

    def stemWords(self, list):
        stems = [self.stemmer.stem(w) for w in list]

        return stems

    def sentTokenize(self):
        tokenized = word_tokenize(self.text)

        filtered = []

        for word in tokenized:
            if word not in self.stop_words:
                if word not in self.punctuation:
                    filtered.append(word)

        return filtered

    def findTags(self):
        filtratedList = self.sentTokenize()

        common = FreqDist(filtratedList).most_common(5)

        roots = self.stemWords(self.topics)

        tags = []

        for word in common:
            for subs in roots:
                if subs in word[0]:
                    tags.append(word[0])

        return tags
