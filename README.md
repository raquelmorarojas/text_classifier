## TEXT CLASSIFIER

This code uses NLTK as main support to obtain the "tags" of an input text depending on the topics you're searching for.
It's a simple way to learn how to use Sentence Tokenization in Python.

Develop by a student in Costa Rican Institute of Technology, January 2020. 
